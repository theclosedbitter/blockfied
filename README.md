# blockfied
A GTK app written in C that lets you block websites and lets you block ads,trackers ,social media and more 

<img src="https://codeberg.org/theclosedbitter/blockfied/raw/branch/main/blockfied.png" alt="blockfied">

# Dependencies
so lets get the obvious out of the way in order to run blockfied you must have GTK and its Dependencies installed just to run and to compile you need to have gcc and make

# Compiling 
to compile blockfied do the following
<br>
git clone https://codeberg.org/theclosedbitter/blockfied.git
<br>
cd blockfied/src/
<br>
chmod +x install.sh
<br>
./install.sh
<br>
sudo ./blockfied

# Root 
in order to run blockfied you have to run it as a root user to use all of its features

# Compatiblity

Only guarnteed to work on linux although you can try running it using mingw but would have to mess with source code to redesign to work for window 